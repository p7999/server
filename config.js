module.exports = {
    sql:{
        user: process.env.DB_USER || 'paygov',
        password: process.env.DB_PWD ||'Aa123456',
        database: process.env.DB_NAME || 'PayGovProd',
        server: process.env.SERVER_HOST || '10.10.248.127',
        pool: {
            max: 10,
            min: 0,
            idleTimeoutMillis: 30000
        },
        options: {
            encrypt: false, // for azure
            trustServerCertificate: false // change to true for local dev / self-signed certs
        }
    }
}
