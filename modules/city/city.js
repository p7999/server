const cities = [
  {
    id: '1',
    name: 'Jerusalem',
    population: 874186
  },
  {
    id: '2',
    name: 'Tel Aviv',
    population: 435855
  },
  {
    id: '3',
    name: 'Ramat Gan',
    population: 153135
  },
  {
    id: '4',
    name: 'Beit Yeoshua',
    population: 1080
  },
];

const getAllCities = () => cities;
const getCityById = (id) => cities.filter((city) => city.id === id);

module.exports = { getAllCities, getCityById };
