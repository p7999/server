const sqlConfig = require('../../config').sql
const sql = require('../../services/sqlserver').Sqlserver
const sqlInstance = new sql(sqlConfig);
const _ = require('lodash')
const crypto =require('crypto')
async function userLogin(req){
    const password = _.get(req,['password']);
    const email = _.get(req,['email']);
    const query = `SELECT PasswordHash FROM PayGovProd.dbo.users WHERE email='${email}'`
    const passHash = crypto.createHash('md5').update(password.toString()).digest('hex');
    const res = await sqlInstance.selectQuery(query);
    const savedPass = _.get(res[0],['PasswordHash'])
    if(passHash===savedPass){
        const userInfoQuery = `SELECT id,isOrganization FROM PayGovProd.dbo.users WHERE email='${email}'`
        const userInfoRes = await sqlInstance.selectQuery(userInfoQuery);
        return userInfoRes[0];
    }
    else {
        return false;
    }
}

module.exports = {
    userLogin
}
