const sqlConfig = require('../../config').sql
const sql = require('../../services/sqlserver').Sqlserver
const sqlInstance = new sql(sqlConfig);
const _ = require('lodash')
const moment = require('moment');
const axios = require('axios')
const crypto = require("crypto");
const paymentsBaseUrl = 'http://paygov-resources.cs.colman.ac.il:5000'
async function userRegistration(req){
    const newUser = _.get(req,['newUser'])
    const password = _.get(newUser,['password'])
    const email = _.get(newUser,['email'])
    const id = _.get(newUser,['id'])
    const name = _.get(newUser,['name'])
    const phone = _.get(newUser,['phone'])
    const city = _.get(newUser,['city'])
    const adress = _.get(newUser,['address'])
    const passHash = crypto.createHash('md5').update(password.toString()).digest('hex');

    const insert = `INSERT INTO PayGovProd.dbo.users (UserId,PasswordHash,email,name,PhoneNumber,Address,City) VALUES ('${id}','${passHash}',
                                        '${email}','${name}','${phone}','${adress}','${city}')`
    await sqlInstance.dmlRequest(insert);
    const selectQueryUserId = `SELECT id FROM PayGovProd.dbo.users WHERE userId='${id}'`
    const execSelect = await sqlInstance.selectQuery(selectQueryUserId);
    await generatePayments(execSelect[0]['id'])
}


async function generatePayments(userId){
    const selectQueryUserId = `SELECT userId FROM PayGovProd.dbo.users WHERE id='${userId}'`
    const execSelect = await sqlInstance.selectQuery(selectQueryUserId);
    const id = _.get(execSelect[0],['userId'])
    const selectOrgApis = `SELECT orgId,BaseApi FROM PayGovProd.dbo.organizations where isAvailable=1`
    const getOrgApisRes = await sqlInstance.selectQuery(selectOrgApis);

    for(const org of getOrgApisRes){
        const baseApi  = _.get(org,['BaseApi'])
        const url = `${paymentsBaseUrl}${baseApi}/${id}`
        const ress = await axios.get(url)
        for(const pay of ress.data){
            const uuid = crypto.randomBytes(16).toString("hex");
            const orgId =_.get(org,['orgId'])
            const amount = _.get(pay,['bill_total'])
            const startDatePayment = _.get(pay,['bill_start_date']);
            const endDatePayment = _.get(pay,['bill_end_date']);

            const description = JSON.stringify(_.get(pay,(['additional'])));
            const isPaid = 0
            const insert = `INSERT INTO PayGovProd.dbo.payments (OrganizationPaymentID,UserID,organizationID,amount,description,startDate,endDate,isPaid) VALUES ('${uuid}',${userId},
                                        '${orgId}','${amount}',N'${(description)}','${moment(startDatePayment).format('YYYY-MM-DD')}','${moment(endDatePayment).format('YYYY-MM-DD')}',${isPaid})`
            console.log(insert);
            await sqlInstance.dmlRequest(insert);
            break;
        }
    }
}


module.exports = {
    userRegistration
}
