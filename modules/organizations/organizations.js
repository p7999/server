const sqlConfig = require('../../config').sql;
const sql = require('../../services/sqlserver').Sqlserver;
const sqlInstance = new sql(sqlConfig);
const { getCityById } = require('../city/city');
const _ = require('lodash');

const queryAmountGroupByOrganizationAndCity = `SELECT count(*) as amount
, CAST(users.City AS NVARCHAR(100)) as cityId
,[organizationID]
FROM [PayGovProd].[dbo].[Payments] as payments
join [PayGovProd].[dbo].[Users] as users on users.UserID = payments.UserID
group by organizationID, CAST(city AS NVARCHAR(100))`;
const dataForPieGraph = [
  {
    organizationID: 1,
    cityId: '2',
    data: [
      { title: 'Before alert', value: 10 },
      { title: 'After first alert', value: 15 },
      { title: 'After second alert', value: 20 },
      { title: 'not paid', value: 20 },
    ],
  },
  {
    organizationID: 1,
    cityId: '3',
    data: [
      { title: 'Before alert', value: 10 },
      { title: 'After first alert', value: 15 },
      { title: 'After second alert', value: 20 },
      { title: 'not paid', value: 20 },
    ],
  },
  {
    organizationID: 1,
    cityId: '4',
    data: [
      { title: 'Before alert', value: 10 },
      { title: 'After first alert', value: 15 },
      { title: 'After second alert', value: 20 },
      { title: 'not paid', value: 20 },
    ],
  },
  {
    organizationID: 1,
    cityId: '1',
    data: [
      { title: 'Before alert', value: 10 },
      { title: 'After first alert', value: 15 },
      { title: 'After second alert', value: 20 },
      { title: 'not paid', value: 20 },
    ],
  },
  {
    organizationID: 2,
    cityId: '4',
    data: [
      { title: 'Before alert', value: 10 },
      { title: 'After first alert', value: 15 },
      { title: 'After second alert', value: 20 },
      { title: 'not paid', value: 20 },
    ],
  },
  {
    organizationID: 3,
    cityId: '1',
    data: [
      { title: 'Before alert', value: 10 },
      { title: 'After first alert', value: 15 },
      { title: 'After second alert', value: 20 },
      { title: 'not paid', value: 20 },
    ],
  },
  {
    organizationID: 3,
    cityId: '3',
    data: [
      { title: 'Before alert', value: 10 },
      { title: 'After first alert', value: 15 },
      { title: 'After second alert', value: 20 },
      { title: 'not paid', value: 20 },
    ],
  },
];

const getDataForPieGraph = async (organizationID, cityId) => {
  let allPayments = await sqlInstance.selectQuery(
    `SELECT [PaymentID], payments.[UserID], [organizationID], [amount], [Payment_Date], [City]
  FROM [PayGov].[dbo].[Payments] as payments
  join [PayGov].[dbo].[Users] as users on users.UserID = payments.UserID`
  );

  let allMessages = await sqlInstance.selectQuery(
    `SELECT id, messages.user_id, [creation_date], [update_date], [type], [status], [City]
    FROM [PayGov].[dbo].message as messages
    join [PayGov].[dbo].[Users] as users on CAST(users.UserID as varchar) like messages.user_id`
  );

  Promise.all([allMessages, allPayments]).then((data) => {
    allMessages = data[0];
    allPayments = data[1];

    let allPaymentsByOrganizationAndCity = allPayments.filter(
      (row) => row.City == cityId && row.organizationID == organizationID
    );
    let allMessagesByOrganizationAndCity = allMessages.filter(
      (row) => row.City == cityId && row.type == organizationID && row.status != 4
    );

    // _.forEach(allPaymentsByOrganizationAndCity, (payment)=> {

    // });
    console.log(allPaymentsByOrganizationAndCity);

    return dataForPieGraph;
  });

  return dataForPieGraph.filter((row) => row.organizationID == organizationID && row.cityId === cityId);
};

const getDataForBarChart = async (organizationID, cityId) => {
  const dataOrganizationAndCityAmount = await sqlInstance.selectQuery(queryAmountGroupByOrganizationAndCity);

  let filterByOrganization = dataOrganizationAndCityAmount.filter(
    (row) => row.organizationID == organizationID && row.cityId === cityId
  );
  filterByOrganization.forEach((row) => {
    row.generalPopulation = getCityById(cityId)[0].population;
  });

  return filterByOrganization;
};

module.exports = { getDataForPieGraph, getDataForBarChart };
