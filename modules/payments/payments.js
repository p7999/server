const sqlConfig = require('../../config').sql
const sql = require('../../services/sqlserver').Sqlserver
const sqlInstance = new sql(sqlConfig);
const _ = require('lodash')


async function userPayAction(req){
    const OrganizationPaymentID = _.get(req,['OrganizationPaymentID'])
    const updateIsPaid = `UPDATE PayGovProd.dbo.payments SET isPaid=1 WHERE OrganizationPaymentID = '${OrganizationPaymentID}'`
    await sqlInstance.dmlRequest(updateIsPaid);
    return {isPaid:true}
}


async function getAllUserPayments(userId){
    const allPayments = await getPaymentsFromDatabase(userId);
    return allPayments;
}

const getAllPaymentsByUserCity = async (userId) => {
    const query = `select 
o.Name
,p.[OrganizationPaymentID] as id 
,p.[amount] as price
,p.[description] 
,p.[startDate] as receiveDate 
,p.[isPaid] 
from [PayGovProd].[dbo].[Users] u
join [PayGovProd].[dbo].[Payments] p on p.UserID = u.id
join [PayGovProd].[dbo].[organizations] o on p.organizationID = o.orgId
where City like (select City from [PayGovProd].[dbo].[Users] where id = ${userId})
and u.id != ${userId}`
    const data = await sqlInstance.selectQuery(query);

    return groupArrayBy(data,'Name');
};

async function getPaymentsFromDatabase(userId){
    const selectOrgApis = `SELECT  o.Name
                  ,p.[OrganizationPaymentID] as id 
                  ,p.[amount] as price
                  ,p.[description] 
                  ,p.[startDate] as receiveDate 
                  ,p.[isPaid] 
               
              FROM [PayGovProd].[dbo].[Payments] p
              join [PayGovProd].[dbo].[organizations] o 
              on p.organizationID = o.orgId
              where UserId=${userId}`;
    const getCustomerPayments = await sqlInstance.selectQuery(selectOrgApis);
    return groupArrayBy(getCustomerPayments,'Name');
}


function groupArrayBy(arr, groupBy) {

    const grouped = _.mapValues(_.groupBy(arr, groupBy),
        clist => clist.map(car => _.omit(car, groupBy)));
    const formatPayments = []
    for(const type of Object.keys(grouped)){
        const payments = {
            title: type,
            payments: _.get(grouped,type)
        }
        formatPayments.push(payments)
    }

    return formatPayments;
}


module.exports = {
    userPayAction,
    getAllUserPayments,
    getAllPaymentsByUserCity
}
