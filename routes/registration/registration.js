const {userRegistration} = require('../../modules/registration/registration');
const { appendCorsHeaders } = require('../cors/cors');

module.exports = (app) => {
    app.post(
        '/register/user', async (req, res) => {
            appendCorsHeaders(res);
            try{
                await userRegistration(req.body);
                res.status(201).send({})
            }catch (e){
                console.log(e);
                res.sendStatus(424)
            }
        }
    );
}
