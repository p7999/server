const {userLogin} = require('../../modules/login/login');
const { appendCorsHeaders } = require('../cors/cors');

module.exports = (app) => {
    app.post(
        '/login', async (req, res) => {
            appendCorsHeaders(res);
            try{
                const isApprovedInfo = await userLogin(req.body);
                if(isApprovedInfo){
                    res.status(200).send(isApprovedInfo);
                }else {
                    res.status(401).send(false);
                }

            }catch (e){
                console.log(e);
                res.sendStatus(401)
            }
        }
    );
}
