const { getDataForPieGraph, getDataForBarChart } = require('../../modules/organizations/organizations');
const { appendCorsHeaders } = require('../cors/cors');

module.exports = (app) => {
  app.get('/organizations/:organizationID/statistics/pieGraph/:cityId', (req, res) => {
    console.log(
      'Organization-pieGraph ' + req.params.organizationID + ' for a city with an id of: ' + req.params.cityId
    );
    let data = getDataForPieGraph(req.params.organizationID, req.params.cityId);

    Promise.all([data]).then((data) => {
      appendCorsHeaders(res);
      res.send(data[0]);
    });
  });

  app.get('/organizations/:organizationID/statistics/barChart/:cityId', async (req, res) => {
    console.log(
      'Organization-barChart ' + req.params.organizationID + ' for a city with an id of: ' + req.params.cityId
    );

    let data = await getDataForBarChart(req.params.organizationID, req.params.cityId);

    Promise.all([data]).then((data) => {
      appendCorsHeaders(res);
      res.send(data[0]);
    });
  });
};
