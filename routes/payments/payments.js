const {userPayAction, getAllUserPayments, getAllPaymentsByUserCity} = require('../../modules/payments/payments');
const { appendCorsHeaders } = require('../cors/cors');

module.exports = (app) => {
    app.post(
        '/payments/pay', async (req, res) => {
            try{
                const resPay = await userPayAction(req.body);
                appendCorsHeaders(res);
                res.status(201).send(resPay)
            }catch (e){
                console.log(e);
                res.sendStatus(424)
            }
        }
    );

    app.get(
        '/payments/getAllPaymentsByUserCity/:userId', async (req, res) => {
            try{
                const allPaymentsByUserCity = await getAllPaymentsByUserCity(req.params.userId);
                Promise.all(allPaymentsByUserCity).then((data) => {
                    appendCorsHeaders(res);
                    res.status(200).send(data);
                });
            }catch (e){
                res.sendStatus(424)
            }
        }
    );

    app.get('/payments/:userId', async (req, res) => {
        const userId = req.params.userId;
        appendCorsHeaders(res);
        res.send(await getAllUserPayments(userId));
    });
}



