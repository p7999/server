const { getAllCities, getCityById } = require('../../modules/city/city');
const { appendCorsHeaders } = require('../cors/cors');

module.exports = (app) => {
  app.get('/cities/', (req, res) => {
    console.log('Getting all cities');
    const data = getAllCities();
    appendCorsHeaders(res);
    res.send(data);
  });

  app.get('/cities/:id', (req, res) => {
    console.log('Getting a city with an id of: ' + req.params.id);
    const data = getCityById(req.params.id);
    appendCorsHeaders(res);
    res.send(data);
  });
};
