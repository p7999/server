const express = require('express');
require('dotenv').config();
const bodyParser = require('body-parser');
const loginRoute = require('./routes/login/login')
const organizationsRoute = require('./routes/organizations/organizationsRoute');
const registerRoute = require('./routes/registration/registration');
const cityRoutes = require('./routes/city/cityRoutes');
const paymentsRoutes = require('./routes/payments/payments');

const app = express();
const port = 8090;
app.use(bodyParser.json());
organizationsRoute(app);
registerRoute(app)
loginRoute(app)
cityRoutes(app);
paymentsRoutes(app)

app.listen(port, () => {
  console.log(`PayGov server listening on port ${port}`);
});
